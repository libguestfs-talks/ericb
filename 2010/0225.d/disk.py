#!/usr/bin/python

import guestfs
g = guestfs.GuestFS ()
g.add_drive_ro ("disk.img")
g.launch ()

parts = g.list_partitions ()
print "disk partitions: %s" % (", ".join (parts))
