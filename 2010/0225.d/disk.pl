#!/usr/bin/perl -w

use strict;

use Sys::Guestfs;

my $g = Sys::Guestfs->new ();
$g->add_drive_ro ("disk.img");
$g->launch ();

my @logvols = $g->lvs ();
print "logical volumes: ", join (", ", @logvols), "\n\n";

$g->mount_ro ("/dev/vg_f12x32/lv_root", "/");
print "----- ISSUE file: -----\n";
print ($g->cat ("/etc/issue"));
print "----- end of ISSUE file -----\n\n";

# Use Augeas to list the NTP servers.
$g->aug_init ("/", 16);
my @nodes = $g->aug_match ("/files/etc/ntp.conf/server");
my @ntp_servers = map { $g->aug_get ($_) } @nodes;
print "NTP servers: ", join (", ", @ntp_servers), "\n\n";
