#!/bin/bash -
source functions
cd 0270.d
add_history fusermount -u mnt
add_history hivexsh SOFTWARE
add_history cd mnt/Windows/System32/config
add_history guestmount --ro -a /dev/vg_thinkpad/Windows7x32 -m /dev/sda2 -o nonempty mnt/
add_history virt-list-filesystems -al /dev/vg_thinkpad/Windows7x32
add_history ls -l /dev/vg_thinkpad
terminal --title="Mount disk image locally"
