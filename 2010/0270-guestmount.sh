#!/bin/bash -
source functions
cd 0270.d
add_history fusermount -u mnt
add_history guestmount --ro -a disk.img -m /dev/vg_f12x32/lv_root -o nonempty mnt/
add_history virt-list-filesystems -al disk.img
add_history ls -l
terminal --title="Mount disk image locally"
