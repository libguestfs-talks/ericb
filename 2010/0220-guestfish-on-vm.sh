#!/bin/bash -
source functions
cd images
add_history guestfish -a disk.img
add_history ls -hs
terminal --title="Guestfish with a local disk image"
