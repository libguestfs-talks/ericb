#!/bin/bash -
source functions
cd empty
add_history virt-win-reg Windows7x32 "'\HKEY_LOCAL_MACHINE\System\ControlSet001\Control'"
add_history virt-win-reg Windows7x32 "'\HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion'" ProductName "'\HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion'" CurrentVersion "'\HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion'" CurrentBuild "'\HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion'" RegisteredOwner
add_history virsh list --all
terminal --title="Examine the Windows Registry"
