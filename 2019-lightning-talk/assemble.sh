#!/bin/bash -

f=$tmpdir/source.asm
s=$tmpdir/final.asm
b=$tmpdir/binary

case "$1" in
    config)
        if [ "$2" = "file" ]; then
            ln -sf "$(realpath "$3")" $f
        else
            echo "unknown parameter $2=$2" >&2
            exit 1
        fi ;;
    config_complete)
        echo 'org 07c00h' > $s
        cat $f >> $s
        echo 'times 510-($-$$) db 0' >> $s
        echo 'db 055h,0aah' >> $s
        nasm -f bin $s -o $b ;;
    get_size) echo 512 ;;
    pread) dd if=$b skip=$4 count=$3 iflag=count_bytes,skip_bytes ;;
    can_write) ;;
    pwrite) exit 1 ;;
    *) exit 2 ;;
esac
