#!/usr/bin/perl -w

use strict;
#use Sys::Virt;
use Sys::Guestfs;

my $vm = "dojo";
my $newpw = "1234567";

my $salt;
my @chars = ("A".."Z", "a".."z", "0".."9", ".", "/");
$salt .= $chars[rand @chars] for 1..16;
my $crypted = crypt ($newpw, '$5$' . $salt . '$');

my $g = Sys::Guestfs->new ();
$g->set_trace (1);
$g->add_domain ($vm, libvirturi => "qemu:///session");
$g->launch ();
$g->mount ("/dev/fedora/root", "/");

my @shadow = $g->read_lines ("/etc/shadow");
s/^root:.*?:/root:$crypted:/ foreach @shadow;

$g->write ("/etc/shadow", join ("\n", @shadow) . "\n");
$g->chmod (0, "/etc/shadow");

$g->touch ("/.autorelabel");
