#!/usr/bin/python

import re
import sys
import guestfs

if len (sys.argv) != 2:
    raise (Error ("disk [image]"))
disk = sys.argv[1]

g = guestfs.GuestFS ()
g.add_drive_opts (disk, readonly=1)

g.launch ()

roots = g.inspect_os ()
if len (roots) == 0:
    raise (Error ("inspect_vm: no operating systems found"))

for root in roots:
    mps = g.inspect_get_mountpoints (root)
    def compare (a, b):
        if len(a[0]) > len(b[0]):
            return 1
        elif len(a[0]) == len(b[0]):
            return 0
        else:
            return -1
    mps.sort (compare)
    for mp_dev in mps:
        try:
            g.mount_ro (mp_dev[1], mp_dev[0])
        except RuntimeError as msg:
            print "%s (ignored)" % msg

    apps = g.inspect_list_applications (root)
    for app in apps:
        if re.search ('firefox', app['app_name'], re.I):
            print ("%s: Firefox version %s" %
                   (disk, app['app_version']))

    g.umount_all ()
