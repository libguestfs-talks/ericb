#!/usr/bin/python

import os
import guestfs

input = "libguestfs-1.16.2.tar.gz"
output = "disk.img"

os.system ("qemu-img create -f raw %s 512M >/dev/null 2>&1" %
           output)

g = guestfs.GuestFS ()
g.add_drive_opts (output, format = "raw", readonly = 0)

g.launch ()

g.part_disk ("/dev/sda", "mbr")
g.mkfs ("ext4", "/dev/sda1")
g.mount ("/dev/sda1", "/")
g.tgz_in (input, "/")

g.close ()
