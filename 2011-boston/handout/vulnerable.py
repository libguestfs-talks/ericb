#!/usr/bin/python

import libxml2, re, sys
from distutils import version

doc = libxml2.readFd (sys.stdin.fileno(), None, None, 0)
ctx = doc.xpathNewContext()
res = ctx.xpathEval ("//application")
for node in res:
    ctx.setContextNode(node)
    name = ctx.xpathEval ("./name//text()")[0]
    ver = ctx.xpathEval ("./version//text()")[0]
    ver = version.StrictVersion (str(ver).split(' ')[0])
    if re.search ("Mozilla Firefox", str(name)) and \
            ver < version.StrictVersion ("3.6.16"):
        print "Vulnerable version of Firefox found (%s)!" % ver
