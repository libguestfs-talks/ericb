#!/bin/bash -

source functions

remember virt-df -h -d FedoraSmall
#remember virt-filesystems --all --long -h -d FedoraSmall
remember sudo lvcreate -L 8G -n FedoraBig vg_pin
remember virt-resize --resize /dev/sda1=+200M --expand /dev/sda2 --lv-expand /dev/vg_fedorasmall/lv_root /dev/vg_pin/FedoraSmall /dev/vg_pin/FedoraBig
remember sudo virsh edit FedoraSmall
remember sudo virsh start FedoraSmall '&&' sudo virt-viewer FedoraSmall '&'

eog virt-resize-intro.png &

terminal --title="virt-resize"
