#!/bin/bash -

source functions

remember 'time virt-inspector -d RHEL60x64 > xml'
remember 'highlight < xml | less -r'
remember "xpath '//product_name' < xml | highlight"
remember "xpath '//application[name=\"firefox\"]' < xml | highlight"

cd 700.d

terminal --title="virt-inspector"
