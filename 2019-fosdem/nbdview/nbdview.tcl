#!/usr/bin/env wish
# Visualize nbdkit activity - read the README file first!
# Copyright (C) 2018 Red Hat Inc.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# * Neither the name of Red Hat nor the names of its contributors may be
# used to endorse or promote products derived from this software without
# specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY RED HAT AND CONTRIBUTORS ''AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL RED HAT OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
# OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

package require Tk

source "nbdcanvas.tcl"

set blocksize 4096
set width 128
set scale 4

# This is used to store the canvas handle.
set h ""

# This function polls the log file (every 100 ms).
proc poll {} {
    global h

    nbdpoll $h
    after 100 poll
}

# Parse command line.
if { $argc != 2 } {
    puts "usage: nbdview logfile size"
    exit 1
}

set logfile [lindex $argv 0]
set size [lindex $argv 1]

# Menubar.
option add *tearOff 0
menu .menubar
. configure -menu .menubar
menu .menubar.file
.menubar add cascade -menu .menubar.file -label File
.menubar.file add command -label "Quit" -command { exit }

# Frame.
frame .f
pack .f -anchor center -expand 1 -fill both -ipadx 10 -ipady 10

# Canvas.
set h [nbdcanvas .f.c $logfile $size $blocksize $width $scale]
.f.c configure -borderwidth 4 -relief groove
pack .f.c -anchor center -expand 0 -fill none

# Set up the window.
wm attributes . -topmost 1
wm title . "nbdview $logfile"
wm resizable . false false

# Start polling the log file.
after 100 poll
