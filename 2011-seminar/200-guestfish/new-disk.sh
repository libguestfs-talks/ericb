#!/bin/bash -

guestfish --rw -x <<EOF

  sparse new-disk.img 10G
  run

  part-init /dev/sda mbr
  part-add /dev/sda primary 2048 524287
  part-add /dev/sda primary 524288 -1

  mkfs ext4 /dev/sda1

  pvcreate /dev/sda2
  vgcreate VG /dev/sda2
  lvcreate swap VG 256
  lvcreate root VG 8192

  mkswap /dev/VG/swap
  mkfs ext4 /dev/VG/root

  mount-options "" /dev/VG/root /
  mkdir /boot
  mount-options "" /dev/sda1 /boot

  txz-in filesystem.tar.xz /

  write /etc/HOSTNAME "newguest.example.com"

EOF
