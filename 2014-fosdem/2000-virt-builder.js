
// poor man's file include
document.write('<h1>Build virtual machines in seconds</h1>')
document.write('<pre>')
document.write('virt-builder centos-6                  \\ <br/>');
document.write('  --format qcow2 --size 20G            \\ <br/>');
document.write('  --hostname cent6                     \\ <br/>');
document.write('  --install @XFCE,cloud-init           \\ <br/>');
document.write('  --edit \'/etc/sysconfig/keyboard:     \\ <br/>');
document.write('      s/^KEYTABLE=.*/KEYTABLE="uk"/\'   \\ <br/>');
document.write('  --firstboot-command \'yum -y update\'  \\ <br/>');
document.write('</pre>');
