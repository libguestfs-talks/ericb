#!/bin/bash -

guests=( $(
  virt-builder -l | awk '{print $1}'
) )
formats=( raw qcow2 )
count=0

while true; do
    guest=${guests[$(($RANDOM % ${#guests[*]}))]}
    format=${formats[$(($RANDOM % ${#formats[*]}))]}

    ((count++))
    echo -e "\n*** Building guest # $count\n"

    virt-builder $guest --output $guest.img \
        --format $format \
        --no-sync \
        --hostname test$count.example.com \
        --timezone Europe/Brussels \
        --write '/etc/motd:
            Welcome to FOSDEM 2014'

    rm $guest.img; # Save space!
done
