function plugins ()
{
    document.write ("\
<div class=\"plugins\"> \
<p id=\"caption\">plugins available in nbdkit 1.12</p> \
<ul> \
<li id=\"plugin-curl\"> curl \
<li id=\"plugin-data\"> data \
<li id=\"plugin-ext2\"> ext2 \
<li id=\"plugin-file\"> file \
<li id=\"plugin-floppy\"> floppy \
<li id=\"plugin-full\"> full \
<li id=\"plugin-guestfs\"> guestfs \
<li id=\"plugin-gzip\"> gzip \
<li id=\"plugin-iso\"> iso \
<li id=\"plugin-libvirt\"> libvirt \
<li id=\"plugin-linuxdisk\"> linuxdisk \
<li id=\"plugin-lua\"> lua \
<li id=\"plugin-memory\"> memory \
<li id=\"plugin-nbd\"> nbd \
<li id=\"plugin-null\"> null \
<li id=\"plugin-ocaml\"> ocaml \
<li id=\"plugin-partitioning\"> partitioning \
<li id=\"plugin-pattern\"> pattern \
<li id=\"plugin-perl\"> perl \
<li id=\"plugin-python\"> python \
<li id=\"plugin-random\"> random \
<li id=\"plugin-ruby\"> ruby \
<li id=\"plugin-rust\"> rust \
<li id=\"plugin-sh\"> sh \
<li id=\"plugin-split\"> split \
<li id=\"plugin-ssh\"> ssh \
<li id=\"plugin-streaming\"> streaming \
<li id=\"plugin-tar\"> tar \
<li id=\"plugin-tcl\"> tcl \
<li id=\"plugin-vddk\"> vddk \
<li id=\"plugin-zero\"> zero \
</ul> \
</div> \
");
}

function filters (layer)
{
    document.write ("\
<div class=\"filters\"> \
<p class=\"filtercaption\">filters available in nbdkit 1.12</p> \
<ul> \
<li id=\"filter" + layer + "-blocksize\"> blocksize \
<li id=\"filter" + layer + "-cache\"> cache \
<li id=\"filter" + layer + "-cow\"> cow \
<li id=\"filter" + layer + "-delay\"> delay \
<li id=\"filter" + layer + "-error\"> error \
<li id=\"filter" + layer + "-fua\"> fua \
<li id=\"filter" + layer + "-log\"> log \
<li id=\"filter" + layer + "-noextents\"> noextents \
<li id=\"filter" + layer + "-nozero\"> nozero \
<li id=\"filter" + layer + "-offset\"> offset \
<li id=\"filter" + layer + "-partition\"> partition \
<li id=\"filter" + layer + "-rate\"> rate \
<li id=\"filter" + layer + "-readahead\"> readahead \
<li id=\"filter" + layer + "-truncate\"> truncate \
<li id=\"filter" + layer + "-xz\"> xz \
</ul> \
</div> \
");
}
