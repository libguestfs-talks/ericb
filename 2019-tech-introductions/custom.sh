#!/bin/bash -

case "$1" in
    get_size)
        echo 1M ;;
    pread)
        for i in `seq 1 $(( $3/16 ))`; do
            echo -ne 'HELLO BEIJING __'
        done
        ;;
    *)
        exit 2;;
esac
